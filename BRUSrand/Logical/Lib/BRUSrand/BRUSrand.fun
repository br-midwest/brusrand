
FUNCTION randGauss : REAL (*Returns a random integer in the inclusive range -1..1 with pseudo-normal distribution using the central limit theorem*)
END_FUNCTION

FUNCTION randPct : REAL (*Uses c rand to generate a random float in the inclusive range 0..1*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
END_FUNCTION

FUNCTION randDigit : USINT (*Returns a random number in the range 0..9 inclusive*)
END_FUNCTION

FUNCTION randLetter : USINT (*Returns random ascii character in the range A..z (uppercase A through lowercase z) inclusive. If pointer to a string is provided, a single random character will be placed in the string. If the pointer is set to 0, the function will return the ASCII code for the random character*)
	VAR_INPUT
		pStr : REFERENCE TO STRING[1]; (* *) (* *) (*#PAR*)
	END_VAR
END_FUNCTION

FUNCTION randInit : USINT (*Call this function in the INIT program once in order to set the seed. If this function is not called, all rand functions will always start from 0 seed on warm restart.*)
END_FUNCTION
