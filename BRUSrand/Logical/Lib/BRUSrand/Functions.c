
#include <bur/plctypes.h>
#include <math.h>

#define MAX_LONG 2147483648.0
#define ASCII_A 65
#define ASCII_a 97

#ifdef __cplusplus
	extern "C"
	{
#endif
#include "BRUSrand.h"
#ifdef __cplusplus
	};
#endif

unsigned char randInit(void)
{
	DTGetTime_typ DTGetTime_0;
	
	DTGetTime(&DTGetTime_0);
	
	srand(DTGetTime_0.DT1);
}

float randPct(void)
{
	long randomLONG = abs(rand());
	return (float)((double)randomLONG / (double)MAX_LONG);
}

float randsum(unsigned int n)
{
	float sum = 0.0;
	int i;
	
	for(i=0; i<n; i++)
	{
		sum += randPct();
	}
	
	return sum;
}

float randGauss(void)
{
	return (randsum(4) / 2.0) - 1.0;
}

unsigned char randDigit(void)
{
	return (unsigned char)(randPct() * 10.0);
}

unsigned char randLetter(plcstring** pStr)
{
	int offset = (randPct() >= 0.5) ? ASCII_A : ASCII_a;
	unsigned char c = ((unsigned char)(randPct() * 26.0) + offset);
	
	if (pStr == 0){
		return c;
	}else{
		*pStr = c;
	}
}